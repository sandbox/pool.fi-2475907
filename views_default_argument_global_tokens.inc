<?php

/**
 * @file
 * Global tokens argument default plugin.
 */

/**
 * Global tokens argument default handler.
 */
class views_default_argument_global_tokens extends views_plugin_argument_default {
  function option_definition() {
    $options = parent::option_definition();
    $options['argument'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['argument'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => $this->options['argument'],
    );
    $form['token_help'] = array(
      '#theme' => 'token_tree',
      '#global_types' => TRUE,
    );
  }

  /**
   * Return the default argument.
   */
  function get_argument() {
    return token_replace($this->options['argument'], array(), array('clear' => TRUE));
  }
}
