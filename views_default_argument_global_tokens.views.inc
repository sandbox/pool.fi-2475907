<?php

/**
 * @file
 * Views integration for views_default_argument_global_tokens.
 */

/**
 * Implements hook_views_plugins().
 */
function views_default_argument_global_tokens_views_plugins() {
  return array(
    'argument default' => array(
      'global_tokens' => array(
        'title' => t('Global tokens'),
        'handler' => 'views_default_argument_global_tokens',
      ),
    ),
  );
}
